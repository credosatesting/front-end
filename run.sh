#!/bin/bash

if [ -n "${DH}" ]; then
   echo "Stored Value = ${DH}"
else
   echo "Setting localhost to DH Variable"
   DH="$("tcp://127.0.0.1:1234")"
fi


HOST_IP="$(echo "$DH" | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")"
echo "Host Ip is = $HOST_IP"
VALUE="var url = 'http://$HOST_IP:5000';"
echo $VALUE > /usr/local/apache2/htdocs/docker.config.js

httpd-foreground
