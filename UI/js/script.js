// script.js

var credoApp = angular.module('credoApp', ['ngRoute', 'ngCookies']);

    // configure our routes
credoApp.config(function($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'pages/home.html',
            controller  : 'mainController',
            activetab: 'home'
        })
                // route for the about page
        .when('/dashboard', {
            templateUrl : 'pages/dashboard.html',
            controller  : 'dashboardController',
            activetab: 'dashboard'
        })

        // route for the about page
        .when('/login', {
            templateUrl : 'pages/login.html',
            controller  : 'loginController',
            activetab: 'login'
        })



        // route for the contact page
        .when('/signup', {
            templateUrl : 'pages/signup.html',
            controller  : 'signupController',
            activetab: 'signup'
        });
})


credoApp.controller('mainController', function($rootScope, $scope, $http, $cookies, $route, $location) {
	$scope.$route = $route;
	var user = $cookies.user;
	if (user) {
		$rootScope.user = user.username;
		$scope.greeting =  "Hello " + user.username + ", welcome to the Matrix";
		$rootScope.loggedIn = true;
	} else {
		$scope.greeting = "Please log in...";
		$rootScope.loggedIn = false;
	}
    // create a message to display in our view
    $scope.message = 'By Jethro & JW';

    $rootScope.logout = function() {
    	$rootScope.loggedIn = false;
		$cookies.user= null;
        $location.path('/');

    }
});


credoApp.controller('dashboardController', function($rootScope, $scope, $http, $cookies, $location) {
	$scope.portfolioMessage = "";
	var user = $cookies.user;
	if (user) {
		$http.get(url + "/user/" + user.id + "/portfolios").then(function(response) {
	  		var portfolioData = response.data;
	  		if(portfolioData.length > 0){
	  			$scope.portfolios = portfolioData;
	  		}
	  		else {
	  			$scope.portfolioMessage = "You don't have any portfolios linked to this account!"
	  		}
		  });
		
	} else {
		$location.path('/');
	}

});

credoApp.controller('loginController', function($rootScope, $scope, $http, $location, $cookies) {

	$scope.check = function (username, password) {
		if (username) 
		{
	  		$http.get(url + "/users/?username=" + username).then(function(response) {
	  		var userData = response.data;
	  		if(userData.length > 0){
		  		if (userData[0].password == password) {
		  			$scope.loginMessage = "Great success!";
		  			
  					$cookies.user = userData[0];
  					$rootScope.loggedIn = true;
	  				$location.path('/');
		  		}
		  		else{
		  			$scope.loginMessage = "Incorrect password!"
		  		}
	  		}
	  		else {
	  			$scope.loginMessage = "Account not found, please go register!"
	  		}
		  });
		}

	};
});

credoApp.controller('signupController', function($rootScope, $scope, $http, $location) {

	$scope.signup = function (username, password, firstName, lastName) {

		var user = {
			username : username,
			password : password,
			firstName : firstName,
			lastName : lastName
		}

        $http.post(url + "/users", user)
		.success(function(data, status, headers, config) {
			$location.path('/login');
		}).error(function(data, status, headers, config) {
		   console.log("Fail", status);
		});
	    
    };
});